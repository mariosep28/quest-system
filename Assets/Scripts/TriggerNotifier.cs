using System;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class TriggerNotifier : MonoBehaviour
{
    public Action onTriggerEnter;
    public Action onTriggerExit;

    private SphereCollider _collider;

    private void Awake()
    {
        _collider = GetComponent<SphereCollider>();
        _collider.isTrigger = true;
    }

    public void SetRadius(float radius)
    {
        _collider.radius = radius;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnter?.Invoke();
    }
    
    private void OnTriggerExit(Collider other)
    {
        onTriggerExit?.Invoke();
    }
}
