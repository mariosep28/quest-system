﻿namespace Quest_System
{
    public static class TaskFactory
    {
        public static ITask Create(TaskData taskData)
        {
            ITask task = null;
            
            switch (taskData.TaskType)
            {
                case "ReachLocation":
                    task = new ReachLocationTask(taskData as ReachLocationTaskData);
                    break;
                
                default:
                    task = new EmptyTask(taskData);
                    break;
            }

            return task;
        }
    }
    
    public static class QuestFactory
    {
        public static IQuest Create(QuestData questData)
        {
            return new Quest(questData);
        }
    }
}