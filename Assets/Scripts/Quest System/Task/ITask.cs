﻿namespace Quest_System
{
    public interface ITask
    {
        public TaskState State { get; }
        
        public void Start();
        public void Complete();
    }
}