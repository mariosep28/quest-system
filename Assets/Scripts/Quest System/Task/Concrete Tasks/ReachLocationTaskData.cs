using Quest_System;
using UnityEngine;

[CreateAssetMenu(menuName = "QuestSystem/Task/ReachLocationTask", fileName = "ReachLocationTask")]
public class ReachLocationTaskData : TaskData
{
    public Vector3 locationToReach;
    public float minDistanceToReach;

    public override string TaskType => "ReachLocation";
}