﻿using Quest_System;
using UnityEngine;

public class ReachLocationTask : Task
{
    private ReachLocationTaskData _data;
    
    private TriggerNotifier triggerNotifier;

    public ReachLocationTask(ReachLocationTaskData data) : base(data)
    {
        _data = data;
    }

    public override void Start()
    {
        base.Start();
        
        SetupLocationGo();
    }

    public override void Complete()
    {
        base.Complete();
        
        Object.Destroy(triggerNotifier.gameObject);        
    }


    private void SetupLocationGo()
    {
        GameObject locationToReachGo = new GameObject("LocationToReach");
        locationToReachGo.transform.position = _data.locationToReach;
        triggerNotifier = locationToReachGo.AddComponent<TriggerNotifier>();
        triggerNotifier.SetRadius(_data.minDistanceToReach);

        triggerNotifier.onTriggerEnter += Complete;
    }
}