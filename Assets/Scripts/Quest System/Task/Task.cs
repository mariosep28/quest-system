using Sirenix.OdinInspector;
using UnityEngine;

namespace Quest_System
{
    public abstract class Task: ITask
    {
        private TaskData _data;
        [SerializeField] private TaskState _state = TaskState.Pending;

        public TaskState State => _state;
        
        protected Task(TaskData data)
        {
            _data = data;
        }

        public virtual void Start()
        {
            if(_state == TaskState.InProgress)
                return;
            
            _state = TaskState.InProgress;
        
            _data.OnTaskStarted();
            
            QuestChannel.TaskStarted(this);
        }

        [ShowIf("@_state != TaskState.Completed")]
        [Button("Complete")]
        public virtual void Complete()
        {
            if(_state == TaskState.Completed)
                return;
            
            _state = TaskState.Completed;
        
            _data.OnTaskCompleted();
            
            QuestChannel.TaskCompleted(this);
        }

        //protected virtual void TryGoal()
        
        public void AddOnTaskStartedAction(IAction action)
        {
            _data.AddOnTaskStartedAction(action);
        }
        
        public void AddOnTaskCompletedAction(IAction action)
        {
            _data.AddOnTaskCompletedAction(action);
        }
    }
}