﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Quest_System
{
    public class TaskData : SerializedScriptableObject
    {
        public virtual string TaskType { get; }
        
        [SerializeField] private new string name;
        
        [SerializeField] private List<IAction> onTaskStarted = new List<IAction>();
        [SerializeField] private List<IAction> onTaskCompleted = new List<IAction>();
        
        public virtual void OnTaskStarted()
        {
            onTaskStarted.ForEach(action => action.Execute());
        }

        public virtual void OnTaskCompleted()
        {
            onTaskCompleted.ForEach(action => action.Execute());
        }
        
        public void AddOnTaskStartedAction(IAction action)
        {
            onTaskStarted.Add(action);
        }
        
        public void AddOnTaskCompletedAction(IAction action)
        {
            onTaskCompleted.Add(action);
        }
    }
}