﻿namespace Quest_System
{
    public enum TaskState
    {
        Pending,
        InProgress,
        Completed
    }
}