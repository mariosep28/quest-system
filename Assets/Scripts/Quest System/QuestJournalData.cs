﻿using System.Collections.Generic;
using UnityEngine;

namespace Quest_System
{
    [CreateAssetMenu(menuName = "QuestSystem/QuestJournal", fileName = "QuestJournal")]
    public class QuestJournalData : ScriptableObject
    {
        [SerializeField] private List<QuestData> quests = new List<QuestData>();
        
        public List<QuestData> Quests => quests;
    }
}