﻿using UnityEngine;

namespace Quest_System
{
    [CreateAssetMenu(menuName = "QuestSystem/ScriptableAction", fileName = "ScriptableAction")]
    public abstract class ScriptableAction : ScriptableObject
    {
        public abstract void Execute();
    }


    [CreateAssetMenu(menuName = "QuestSystem/ScriptableAction/Reward", fileName = "Reward")]
    public class Reward : ScriptableObject, IAction
    {
        private string message;
        
        public void Execute()
        {
            Debug.Log(message);
        }
    }
}

