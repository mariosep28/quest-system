﻿namespace Quest_System
{
    public interface IAction
    {
        public void Execute();
    }
}