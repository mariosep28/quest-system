using UnityEngine;

namespace Quest_System
{
    public class QuestManager : Singleton<QuestManager>
    {
        [SerializeField] private QuestJournalData questJournalData;

        [SerializeField] private QuestJournal questJournal;
        
        public QuestJournalData QuestJournalData
        {
            get => questJournalData;
            set => questJournalData = value;
        }

        private void Awake()
        {
            if (questJournalData == null)
                questJournalData = ScriptableObject.CreateInstance<QuestJournalData>();

            questJournal = new QuestJournal(questJournalData);
        }

        public void AssignQuest(QuestData questData)
        {
            IQuest quest = QuestFactory.Create(questData);
            questJournal.AssignQuest(quest);
        }
    }
}

