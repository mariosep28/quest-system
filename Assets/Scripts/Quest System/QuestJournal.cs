﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Quest_System
{
    [Serializable]
    public class QuestJournal
    {
        [SerializeReference] private List<IQuest> pendingQuests = new List<IQuest>();
        [SerializeReference] private List<IQuest> currentQuests = new List<IQuest>();
        [SerializeReference] private List<IQuest> completedQuests = new List<IQuest>();

        public List<IQuest> PendingQuests => pendingQuests;
        public List<IQuest> CurrentQuests => currentQuests;
        public List<IQuest> CompletedQuests => completedQuests;

        public QuestJournal()
        {
            QuestChannel.onQuestCompleted += OnQuestCompleted;
        }
        
        public QuestJournal(QuestJournalData data)
        {
            // TODO: Extract to factory
            foreach (QuestData questData in data.Quests)
            {
                IQuest quest = QuestFactory.Create(questData);
                AssignQuest(quest);
            }

            QuestChannel.onQuestCompleted += OnQuestCompleted;
        }
        
        public void AssignQuest(IQuest quest)
        {
            pendingQuests.Add(quest);
            
            if(quest.CheckRequirements())
                StartQuest(quest);
        }

        private void StartQuest(IQuest quest)
        {
            if (pendingQuests.Contains(quest))
            {
                pendingQuests.Remove(quest);
            }
            
            currentQuests.Add(quest);
            quest.Start();
        }

        private void OnQuestCompleted(IQuest quest)
        {
            if (currentQuests.Contains(quest))
            {
                currentQuests.Remove(quest);
                completedQuests.Add(quest);
            }
        }
    }
}