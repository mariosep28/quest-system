﻿using System;

namespace Quest_System
{
    public static class QuestChannel
    {
        public static Action<IQuest> onQuestStarted;
        public static Action<IQuest> onQuestCompleted;
        public static Action<ITask> onTaskStarted;
        public static Action<ITask> onTaskCompleted;

        public static void QuestStarted(IQuest quest) => onQuestStarted?.Invoke(quest);
        public static void QuestCompleted(IQuest quest) => onQuestCompleted?.Invoke(quest);
        public static void TaskStarted(ITask task) => onTaskStarted?.Invoke(task);
        public static void TaskCompleted(ITask task) => onTaskCompleted?.Invoke(task);
    }
}