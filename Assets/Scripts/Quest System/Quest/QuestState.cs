﻿namespace Quest_System
{
    public enum QuestState
    {
        Pending,
        InProgress,
        Completed
    }
}