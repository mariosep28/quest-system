using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Quest_System
{
    [CreateAssetMenu(menuName = "QuestSystem/Quest", fileName = "Quest")]
    public class QuestData : SerializedScriptableObject 
    {
        [SerializeField] private new string name; 
        
        [SerializeField] private List<TaskData> tasks = new List<TaskData>();
        
        [SerializeField] private List<IAction> onQuestStarted = new List<IAction>();
        [SerializeField] private List<IAction> onQuestCompleted = new List<IAction>();

        [SerializeField] private List<QuestData> unlockedQuestsOnComplete = new List<QuestData>();

        public List<TaskData> Tasks => tasks;
        
        public virtual void OnQuestStarted()
        {
            onQuestStarted.ForEach(action => action.Execute());
        }

        public virtual void OnQuestCompleted()
        {
            onQuestCompleted.ForEach(action => action.Execute());
            
            unlockedQuestsOnComplete.ForEach(questData => QuestManager.Instance.AssignQuest(questData));
        }

        public void AddTask(TaskData taskData)
        {
            tasks.Add(taskData);
        }
        
        public void AddOnQuestStartedAction(IAction action)
        {
            onQuestStarted.Add(action);
        }
        
        public void AddOnQuestCompletedAction(IAction action)
        {
            onQuestCompleted.Add(action);
        }
        
        public void AddUnlockedQuestOnComplete(QuestData questData)
        {
            unlockedQuestsOnComplete.Add(questData);
        }
    }
}