﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Quest_System
{
    [Serializable]
    public class Quest: IQuest
    {
        [SerializeField] private QuestData _data;
        
        [SerializeField] private QuestState _state = QuestState.Pending;
        
        [SerializeReference] private List<ITask> pendingTasks = new List<ITask>();
        [SerializeReference] private List<ITask> currentTasks = new List<ITask>();
        [SerializeReference] private List<ITask> completedTasks = new List<ITask>();

        public QuestState State => _state;
        
        public List<ITask> PendingTasks => pendingTasks;
        public List<ITask> CurrentTasks => currentTasks;
        public List<ITask> CompletedTasks => completedTasks;

        public Quest()
        {
            _data = ScriptableObject.CreateInstance<QuestData>();
        }
        
        public Quest(QuestData data)
        {
            _data = data;

            // TODO: Extract to factory
            foreach (TaskData taskData in data.Tasks)
            {
                ITask task = TaskFactory.Create(taskData); 
                pendingTasks.Add(task);
            }
        }
        
        public void Start()
        {
            if(_state == QuestState.InProgress) 
                return;
            
            _state = QuestState.InProgress;
            
            QuestChannel.onTaskCompleted += OnTaskCompleted;
            
            _data.OnQuestStarted();
            QuestChannel.QuestStarted(this);
            
            if (pendingTasks.Count > 0)
                StartNextTask();
            else
                Complete();
        }

        [ShowIf("@_state != QuestState.Completed")]
        [Button("Complete")]
        public void Complete()
        {
            if(_state == QuestState.Completed) 
                return;
            
            _state = QuestState.Completed;

            QuestChannel.onTaskCompleted -= OnTaskCompleted;
            
            _data.OnQuestCompleted();
            QuestChannel.QuestCompleted(this);
        }

        public bool CheckRequirements() => true;
        
        private void StartNextTask()
        {
            ITask nextTask = pendingTasks.First();
            pendingTasks.Remove(nextTask);
            
            currentTasks.Add(nextTask);
            nextTask.Start();
        }

        private void OnTaskCompleted(ITask task)
        {
            if (!currentTasks.Contains(task)) return;
            
            currentTasks.Remove(task);
            completedTasks.Add(task);

            if (pendingTasks.Count > 0)
                StartNextTask();
            else
                Complete();
        }

        public void AddTask(ITask task)
        {
            pendingTasks.Add(task);
        }

        public void AddOnQuestStartedAction(IAction action)
        {
            _data.AddOnQuestStartedAction(action);
        }
        
        public void AddOnQuestCompletedAction(IAction action)
        {
            _data.AddOnQuestCompletedAction(action);
        }
    }
}