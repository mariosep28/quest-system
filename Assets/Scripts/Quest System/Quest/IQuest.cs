﻿namespace Quest_System
{
    public interface IQuest
    {
        public void Start();
        public void Complete();
        public bool CheckRequirements();
    }
}